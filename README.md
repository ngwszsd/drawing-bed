## 介绍

主要作为图床存储MarkDown需要使用的图片

## 软件架构

UI：element-ui
插件：
Clipboard 复制文本到剪贴板
vue-FilePond 上传文件
数据存储使用localStorage或文件缓存于浏览器本地  

+ Cloudflare Build: npm run build
+ The Build directory: /dist
