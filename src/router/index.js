import Vue from 'vue'
import Router from 'vue-router'
import App from '@/App'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      redirect: '/?xx=8f8c17968dcf926352a71e905a87e8f7&oo=garvinew',
      name: 'App',
      component: App
    }
  ]
})
